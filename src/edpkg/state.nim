import
  nimraylib_now,
  opencolor,
  std/[options, strformat, enumerate, math]

# import nimraylib_now/raygui

func toRlColor*(color: opencolor.Color, a: uint8): nimraylib_now.Color =
  let rgb = color.toRGB
  nimraylib_now.Color(r: rgb.r, g: rgb.g, b: rgb.b, a: a)

converter toRlColor*(color: opencolor.Color): nimraylib_now.Color =
  toRlColor(color, 255)

type
  ElementState* {.pure.} = enum
    normal
    hovered
    dragged


type Node* = object
  pos*: Vector2

const NODE_RADIUS = 16.0

proc draw(this: Node, state: ElementState) =
  let color = case state
  of normal: ocBlue5
  of hovered: ocBlue3
  of dragged: ocBlue6
  drawCircleV(this.pos, NODE_RADIUS, color)

func contains_point(this: Node, point: Vector2): bool =
  (point - this.pos).length <= NODE_RADIUS

func move_by(this: var Node, diff: Vector2) = this.pos += diff


# type
#   ElementKind* {.pure.} = enum
#     circle
#   Element* = object
#     case kind*: ElementKind
#     of circle: circle*: Circle

# converter toElement*(this: Circle): Element = Element(kind: circle, circle: this)

# proc draw(this: Element, state: ElementState) =
#   case this.kind
#   of circle:
#     this.circle.draw(state)

# func pos(this: Element): Vector2 =
#   case this.kind
#   of circle:
#     this.circle.pos

# func contains_point(this: Element, point: Vector2): bool =
#   case this.kind
#   of circle:
#     this.circle.contains_point(point)

# func move_by(this: var Element, diff: Vector2) =
#   case this.kind
#   of circle:
#     this.circle.move_by(diff)


type
  LinkType* {.pure.} = enum
    element
    link
  Linked* = object
    case kind*: LinkType
    of element:
      elementId*: int
    of link:
      linkId*: int
      linkWhere*: float32
        ## 0  = at link.a
        ## 1  = at link.b
        ## 0.5= in the middle
  Link* = object
    a*: Linked
    b*: Linked

func `==`*(l, r: Linked): bool =
  ## doesn't check `.linkWhere`
  l.kind == r.kind and (
    case l.kind
    of element:
      l.elementId == r.elementId
    of link:
      l.linkId == r.linkId
  )

func initLink*(a, b: Linked): Link =
  Link(a:a,b:b)

func linkIdElement*(id: int): Linked =
  Linked(kind: element, elementId: id)

func linkIdLink*(id: int, zero_to_one: float32): Linked =
  Linked(kind: link, linkId: id, linkWhere: zero_to_one)


type State* = object
  dragging*: Option[Linked]
  hovered *: Option[Linked]
  elements*: seq[Node]
  links   *: seq[Link]
  last_clicked*: array[2, Option[Linked]]

func initState*(): State =
  for it in mitems(result.last_clicked):
    it = none(Linked)

func center_pos*(this: State; what: Linked): Vector2

func points(this: State, link: Link): (Vector2, Vector2) =
  (this.center_pos(link.a), this.center_pos(link.b))

func center_pos*(this: State; what: Linked): Vector2 =
  case what.kind
  of element:
    this.elements[what.elementId].pos
  of link:
    let link = this.links[what.linkId]
    let (pos_a, pos_b) = this.points(link)
    let blend = pos_a * (1.0 - what.linkWhere) + pos_b * what.linkWhere
    blend

func contains_point(this: Link, state: State, point: Vector2): Option[float32] =
  let (v0, v1) = state.points(this)
  let diff = (v1 - v0)
  let angle = arctan2(diff.y, diff.x)
  let normalized = (point - v0).rotate(-angle)
  const TOLERANCE = 10.0
  let W = (v1 - v0).length
  let rect = Rectangle(x: 0.0, y: -TOLERANCE, width: W, height: TOLERANCE*2.0)
  if checkCollisionPointRec(normalized, rect):
    some(normalized.x / W)
  else:
    none(float32)

proc drawGui(this: var State) =
  var rect: Rectangle = (0.0, 0.0, 100.0, 20.0)
  label(rect, fmt"last_clicked: {this.last_clicked}".cstring)
  rect.y += 20.0
  label(rect, fmt"links: {this.links}".cstring)
  rect.y += 20.0
  if button(rect, "link"):
    let (a, b) = (this.last_clicked[0], this.last_clicked[1])
    if a.isSome and b.isSome:
      this.links &= initLink(a.get, b.get)
  rect.y += 20.0

  for i, el in enumerate(this.elements):
    let pos = el.pos
    drawText(fmt"id={i}".cstring, pos.x.cint, pos.y.cint, 10, BLACK)


proc draw(link: Link, this: State, state: ElementState) =
  ## draw link
  let color = case state
  of normal: ocBlue5
  of hovered: ocBlue3
  of dragged: ocBlue6
  let thick = case state
  of normal: 1.0
  of hovered: 2.0
  of dragged: 1.0
  let pos_a = this.center_pos(link.a)
  let pos_b = this.center_pos(link.b)
  drawLineEx(pos_a, pos_b, thick, color)

proc drawLinks(this: State) =
  for i, link in enumerate(this.links):
    let shouldbe = some(linkIdLink(i, 0.0))
    let state =
      if this.dragging == shouldbe:
        ElementState.dragged
      elif this.hovered == shouldbe:
        ElementState.hovered
      else:
        ElementState.normal
    link.draw(this, state)


proc drawElements(this: State) =
  for i, el in enumerate(this.elements):
    let shouldbe = some(linkIdElement(i))
    let state =
      if this.dragging == shouldbe:
        ElementState.dragged
      elif this.hovered == shouldbe:
        ElementState.hovered
      else:
        ElementState.normal
    el.draw(state)

proc draw*(this: var State) =
  this.drawLinks()
  this.drawElements()
  this.drawGui()

proc hovering_over(this: State): Option[Linked] =
  let pos = getMousePosition()
  for i, el in enumerate(this.elements):
    if el.contains_point(pos):
      return linkIdElement(i).some
  for i, li in enumerate(this.links):
    let where = li.contains_point(this, pos)
    if where.isSome:
      return linkIdLink(i, where.get).some
  none(Linked)

proc update_dragging(this: var State) =
  this.hovered = this.hovering_over()

  if isMouseButtonPressed(MouseButton.LEFT) and this.hovered.isSome:
    this.dragging = this.hovered
    for i in countdown(this.last_clicked.len-2, 0):
      this.last_clicked[i+1] = this.last_clicked[i]
    this.last_clicked[0] = this.hovered

  if this.dragging.isSome:
    let dragged = this.dragging.get
    case dragged.kind
    of element:
      let diff = getMouseDelta()
      this.elements[dragged.elementId].move_by(diff)
    else:
      discard # todo

  if isMouseButtonReleased(MouseButton.LEFT):
    this.dragging = none(Linked)

proc update*(this: var State) =
  this.update_dragging()
