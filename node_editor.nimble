# Package

version       = "0.1.0"
author        = "Locria Cyber"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["node_editor"]

# Dependencies

requires "nim >= 1.9.1"
requires "nimraylib_now >= 0.15.0"
requires "opencolor >= 1.9.1.1"