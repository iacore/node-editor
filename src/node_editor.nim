import nimraylib_now

import edpkg/state

proc main =
  var state: State = initState()
  state.elements &= Node(pos: Vector2(x: 200, y: 200))
  state.elements &= Node(pos: Vector2(x: 200, y: 400))
  state.elements &= Node(pos: Vector2(x: 400, y: 200))
  state.elements &= Node(pos: Vector2(x: 400, y: 400))
  initWindow(600, 600, "(untitled)")
  setTargetFPS(24)
  while not windowShouldClose():
    state.update()
    beginDrawing()
    clearBackground(WHITE)
    state.draw()
    endDrawing()

when isMainModule:
  main()
